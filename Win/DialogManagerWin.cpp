/*
* Copyright 2012 Ryan Ackley (ryanackley@gmail.com)
*
* This file is part of NPAPI Chrome File API
*
* NPAPI Chrome File API is free software: you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include "win_common.h"
#include <commdlg.h>
#include <string>
#include <boost/thread.hpp>
#include "utf8_tools.h"
#include "Win/PluginWindowlessWin.h"
#include "Win/PluginWindowWin.h"

#include "DialogManagerWin.h"
#include <shlobj.h>
#include "precompiled_headers.h" // Anything before this is PCH on windows

DialogManager* DialogManager::get()
{
    static DialogManagerWin inst;
    return &inst;
}
void DialogManagerWin::OpenFileDialog(const FB::BrowserHostPtr& host, FB::PluginWindow* win, const PathCallback& cb) {
	_showDialog(FALSE, host, win, cb);
}

void DialogManagerWin::OpenFolderDialog(const FB::BrowserHostPtr& host, FB::PluginWindow* win, const PathCallback& cb) {
    _showDialog(TRUE, host, win, cb);
}

void DialogManagerWin::_showDialog(const BOOL isFolder, const FB::BrowserHostPtr& host, FB::PluginWindow* win, const PathCallback& cb) {
	/*FB::PluginWindowWin* wndWin = dynamic_cast<FB::PluginWindowWin*>(win);
    FB::PluginWindowlessWin* wndlessWin = dynamic_cast<FB::PluginWindowlessWin*>(win);

    HWND browserWindow = wndWin ? wndWin->getBrowserHWND() : wndlessWin->getHWND();*/
	HWND browserWindow = GetForegroundWindow();
	if(isFolder)
	{
		boost::thread dlgThread(&DialogManagerWin::_showFolderDialog, this, browserWindow, cb);
	}
	else
	{
		boost::thread dlgThread(&DialogManagerWin::_showFileDialog, this, browserWindow, cb);
	}
}

void DialogManagerWin::_showFolderDialog(HWND wnd, const PathCallback& cb) {
    BROWSEINFO bi = { 0 };
    bi.lpszTitle = _T("Select a folder to import");
    bi.hwndOwner = wnd;
    LPITEMIDLIST pidl = SHBrowseForFolder ( &bi );
    if ( pidl != 0 )
    {
        std::wstring out;
        // get the name of the folder
        TCHAR path[MAX_PATH];
        if ( SHGetPathFromIDList ( pidl, path ) )
        {
            out = path;
        }

        // free memory used
        IMalloc * imalloc = 0;
        if ( SUCCEEDED( SHGetMalloc ( &imalloc )) )
        {
            imalloc->Free ( pidl );
            imalloc->Release ( );
        }
        cb(FB::wstring_to_utf8(path));
    } else {
        cb("");
    }
}
void DialogManagerWin::_showFileDialog(HWND wnd,const PathCallback& cb)
{
    wchar_t Filestring[4096];
    std::string out;

    //std::wstring wFilter(FB::utf8_to_wstring(filter));
    //std::wstring wPath(FB::utf8_to_wstring(filter));

    OPENFILENAME opf;
    opf.hwndOwner = wnd;
    opf.lpstrFilter = NULL;
    opf.lpstrCustomFilter = 0;
    opf.nMaxCustFilter = 0L;
    opf.nFilterIndex = 1L;
    opf.lpstrFile = Filestring;
    opf.lpstrFile[0] = '\0';
    opf.nMaxFile = 4096;
    opf.lpstrFileTitle = 0;
    opf.nMaxFileTitle=50;
    opf.lpstrInitialDir = NULL;
    opf.lpstrTitle = L"Select file";
    opf.nFileOffset = 0;
    opf.nFileExtension = 0;
    opf.lpstrDefExt = L"*.*";
    opf.lpfnHook = NULL;
    opf.lCustData = 0;
    opf.Flags = (OFN_PATHMUSTEXIST | OFN_OVERWRITEPROMPT) & ~OFN_ALLOWMULTISELECT;
    opf.lStructSize = sizeof(OPENFILENAME);

    if(GetOpenFileName(&opf))
    {
        out = FB::wstring_to_utf8(std::wstring(opf.lpstrFile));
    }

    cb(out);
}