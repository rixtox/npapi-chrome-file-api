/*
* Copyright 2012 Ryan Ackley (ryanackley@gmail.com)
*
* This file is part of NPAPI Chrome File API
*
* NPAPI Chrome File API is free software: you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#pragma once

#include "JSObject.h"
#include "windows.h"

class FileWatcherTaskWin
{
public:
	FileWatcherTaskWin(std::string path, FB::JSObjectPtr callback);
	~FileWatcherTaskWin(){};

	void StartWatching();
	void StopWatching();

private:

	static void CALLBACK WatcherCallback(DWORD dwErrorCode,DWORD dwNumberOfBytesTransfered,	LPOVERLAPPED lpOverlapped);

	static void CALLBACK InitDirectoryWatcher(ULONG_PTR arg);

	static void CALLBACK TerminateWatch(ULONG_PTR arg);
	
	void StartAlertLoop();
	BOOL ListenForFileEvents();
	void CloseHandlesStopAlertLoop();
	//bool ShouldStop();
	
	HANDLE m_handle;
	char m_buffer[4096];
	
	bool stop;
	std::wstring m_path;
	FB::JSObjectPtr m_callback;
	OVERLAPPED m_ol;
	boost::thread m_thread;
};
