/*
* Copyright 2012 Ryan Ackley (ryanackley@gmail.com)
*
* This file is part of NPAPI Chrome File API
*
* NPAPI Chrome File API is free software: you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
#include "FileWatcherMac.h"

FileWatcher* FileWatcher::get()
{
    static FileWatcherMac inst;
    return &inst;
}

void FileWatcherMac::WatchFile(std::string key, std::string path, FB::JSObjectPtr callback)
{
    NSAutoreleasePool * pool = [NSAutoreleasePool new];
    FileWatcherTask* task = [[FileWatcherTask alloc] initWithCallback:callback];
    NSString* strPath = [NSString stringWithUTF8String:path.c_str()];
    [task performSelectorInBackground:@selector(listenForFileEvents:) withObject:strPath];
    watchers[key] = task;
    [pool release];
}

void FileWatcherMac::StopWatching(std::string key)
{
    NSAutoreleasePool * pool = [NSAutoreleasePool new];
    FileWatcherTask* task = watchers[key];
    if (task)
    {
        watchers.erase(key);
        [task stopWatching];
        [task release];
    }
    [pool release]; 
}